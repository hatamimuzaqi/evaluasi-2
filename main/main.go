package main

import (
	"fmt"
	"html/template"
	"net/http"
	"path"
	"time"
)

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "apa kabar!")
}

func stringslength(w http.ResponseWriter, r *http.Request){
	const length = "abcdefgh"
	fmt.Fprintln(w, len(length))
}

func ktp(w http.ResponseWriter, r *http.Request)string{
	var ktp = r.FormValue("ktp")
	if len(ktp) <= 16 {
		return ktp
	} else {
		return"Masukkan Nomor KTP Yang Benar"
	}
}

func period(w http.ResponseWriter, r *http.Request)string{
	var period = r.FormValue("period")
	if period >= "2" || period <= "15"{
		return period
	} else {
		return "masukan periode yang benar"
	}
}

func amount(w http.ResponseWriter, r *http.Request)string{
	var amount = r.FormValue("amount")
	if amount >= "2000000" || amount <="12000000"{
		return amount
	} else {
		return "masukan jumlah pinjaman yang benar"
	}
}

func timenow(w http.ResponseWriter, r *http.Request) string{
	t := time.Now()

	var timenow = t.Format("2006-01-02")

	return timenow
}

func birthdate(w http.ResponseWriter, r *http.Request) string{
	layout := "2006-01-02"
	str := r.FormValue("birthdate")
	t, err := time.Parse(layout, str)

	if err != nil {
		fmt.Println(err)
	}
	var birthdate = t.Format(layout)
	return birthdate

}
func form(w http.ResponseWriter, r *http.Request){
	if r.Method == "GET" {
		var filepath = path.Join("C:/Goproject/src/zaki/hello/views", "view.html")
		var tmpl = template.Must(template.New("form").ParseFiles(filepath))
		var err = tmpl.Execute(w, nil)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		return
	}
}

func parseformnull(w http.ResponseWriter, r *http.Request){
	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func submit(w http.ResponseWriter, r *http.Request){
	if r.Method == "POST" {
		var filepath = path.Join("C:/Goproject/src/zaki/hello/views", "view.html")
		var tmpl = template.Must(template.New("result").ParseFiles(filepath))

		parseformnull(w,r)

		timenow := timenow(w, r)
		ktp := ktp(w, r )
		birth := birthdate(w,r)
		var gender = r.FormValue("gender")
		var name = r.FormValue("nama")
		amount := amount(w,r)
		period := period(w,r)
		var data = map[string]string{"ktp":ktp, "birth":birth,"gender" : gender, "name" : name,"amount":amount,"period":period,"timenow":timenow, "title":"Learn Form Golang"}

		if err := tmpl.Execute(w, data); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var filepath = path.Join("C:/Goproject/src/zaki/hello/views", "view.html")
		var tmpl, err = template.ParseFiles(filepath)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var data = map[string]interface{}{
			"title" : "Form Golang",
		}

		err = tmpl.Execute(w, data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	http.HandleFunc("/index", index)

	http.HandleFunc("/string", stringslength)

	http.HandleFunc("/form", form)

	http.HandleFunc("/submit", submit)

	http.Handle("/static/",
		http.StripPrefix("/static/",
			http.FileServer(http.Dir("C:/Goproject/src/zaki/hello/assets"))))

	fmt.Println("server started at localhost:8080")
	http.ListenAndServe(":8080", nil)
}